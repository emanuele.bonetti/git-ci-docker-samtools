FROM ubuntu:18.04

RUN apt-get update && \
	apt-get install -y samtools && \
	apt-get install -y bwa
